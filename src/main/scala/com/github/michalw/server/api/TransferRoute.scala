package com.github.michalw.server.api

import scala.concurrent.ExecutionContextExecutor
import scala.language.implicitConversions
import scala.language.postfixOps
import scala.util.Success
import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.StatusCodes
import akka.pattern.ask
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import com.github.michalw.domain.coordinator.TransferProcess
import com.github.michalw.domain.coordinator.TransferProcessEvent.InitTransfer
import com.github.michalw.models._
import com.github.michalw.models.messages.coordinator._
import com.github.michalw.utils.TransactionIdGen
import com.github.michalw.utils.config.Environment


trait TransferRoute
  extends JsonMappings
    with TransactionIdGen {

  def config: Environment
  def accountService: ActorRef
  implicit def system: ActorSystem
  implicit def ec: ExecutionContextExecutor
  private implicit def timeout: Timeout = config.server.timeout

  val transfer: Route =
    path("transfer") {
      post {
        entity(as[TransferRequest]) {
          case tr: TransferRequest if tr.from == tr.to =>
            complete(StatusCodes.BadRequest, PreTransferError("cannot transfer funds between same account"))
          case tr: TransferRequest if tr.funds <= 0 =>
            complete(StatusCodes.BadRequest, PreTransferError("only positive value can be transferred between accounts"))
          case tr: TransferRequest =>
            val tId = getTnxId
            val transferProcess = TransferProcess(config.transferProcess, accountService)
            val result = transferProcess ? InitTransfer(tId, tr)
            onComplete(result) {
              case Success(Transferred(x)) if x == tId =>
                  complete(StatusCodes.OK, TransferResponse(tId))
              case Success(TransferFailed(x, error@NotEnoughFundsError)) if x == tId =>
                complete(StatusCodes.MethodNotAllowed, TransferResponse(tId, Option(error.getMessage)))
              case Success(TransferFailed(x, error@ClientNotFoundError)) if x == tId =>
                complete(StatusCodes.NotFound, TransferResponse(tId, Option(error.getMessage)))
              case Success(TransferFailed(x, error)) if x == tId =>
                complete(StatusCodes.InternalServerError, TransferResponse(tId, Option(error.getMessage)))
            }
          case _ => failWith(new RuntimeException("Something wen't wrong"))
        }
      }
    }

}
