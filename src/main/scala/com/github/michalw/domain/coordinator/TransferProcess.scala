package com.github.michalw.domain.coordinator

import akka.actor.{ActorLogging, ActorRef, ActorSystem, Props}
import com.github.michalw.domain.coordinator.TransferProcessData.{NoData, ProcessingData}
import com.github.michalw.domain.coordinator.TransferProcessEvent._
import com.github.michalw.domain.coordinator.TransferProcessState._
import com.github.michalw.models.TransferRequest
import com.github.michalw.models.messages.account.{Credit, Debit, Rollback, Validate}
import com.github.michalw.models.messages.coordinator.{ValidClient, _}
import com.github.michalw.utils.config.TransferProcessConfig


/**
  * Actor based Transfer processor coordinates money transfer between different accounts.
  * uses state machine to handle it correctly. It is created per transfer request and uses tell pattern
  * @param config various configuration properties (ex. tiemouts)
  * @param accountService actor that handler concrete account service
  */
class TransferProcess(private val config: TransferProcessConfig, private val accountService: ActorRef)
  extends TransferProcessFSM
    with ActorLogging {

  private val maxRetryCount: Int = config.maxRetryCount
  startWith(Uninitialized, NoData)

  when(Uninitialized) {
    case Event(InitTransfer(tId, TransferRequest(from, to, amount)), NoData) =>
      log.info("InitTransfer:" + tId)
      log.debug("transfer[" + tId + "]details[from=" + from + ",to=" + to + ",amount=" + amount)
      accountService ! Validate(tId, Seq(from, to), self)
      goto(AwaitingValidationConfirmation) using ProcessingData(tId, from, to, amount, 0, sender())
  }

  when(AwaitingValidationConfirmation, config.validation.timeout) {
    case Event(AccountServiceResponse(ValidClient), _) =>
      val pd@ProcessingData(tId, from, _, amount, _, _) = stateData
      log.info("Validation OK:" + tId)
      accountService ! Debit(tId, from, amount, self)
      goto(AwaitingDebitConfirmation) using pd
    case Event(AccountServiceResponse(ClientNotFound), _) =>
      val ProcessingData(tId, _, _, _, _, caller) = stateData
      log.info("Credited:" + tId)
      caller ! TransferFailed(tId, ClientNotFoundError)
      stop()
  }

  when(AwaitingDebitConfirmation, config.debit.timeout) {
    defaultHandler{
      case Event(AccountServiceResponse(Debited), _) =>
        val pd@ProcessingData(tId, _, to, amount, _, _) = stateData
        log.info("Debited:" + tId)
        accountService ! Credit(tId, to, amount, self)
        goto(AwaitingCreditConfirmation) using pd.copy(retryCount = 0)
      case Event(AccountServiceResponse(NotEnoughFunds), _) =>
        val ProcessingData(tId, _, _, _, _, caller) = stateData
        log.info("Credited:" + tId)
        caller ! TransferFailed(tId, NotEnoughFundsError)
        stop()
    } {
      defaultRetryHandler{
        pd => {
          val ProcessingData(tId, from, _, amount, retryCount, _) = pd
          log.debug("Debit Failed, retrying...[retry=" + {retryCount + 1} + "]:" + tId)
          accountService ! Debit(tId, from, amount, self)
          stay() using pd.copy(retryCount = retryCount + 1)
        }
      }
    }
  }

  when(AwaitingCreditConfirmation, config.credit.timeout) {
    defaultHandler {
      case Event(AccountServiceResponse(Credited), _) =>
        val ProcessingData(tId, _, _, _, _, caller) = stateData
        log.info("Credited:" + tId)
        caller ! Transferred(tId)
        stop()
    } {
      case Event(AccountServiceResponse(_), _) =>
        stateData match {
          case pd@ProcessingData(tId, _, to, amount, retryCount, _) if retryCount < maxRetryCount =>
            log.debug("Credit Failed, retrying...[retry=" + {
              retryCount + 1
            } + "]:" + tId)
            accountService ! Credit(tId, to, amount, self)
            stay() using pd.copy(retryCount = retryCount + 1)
          case pd@ProcessingData(tId, from, _, amount, _, _) =>
            log.error("Credit failed:" + tId)
            accountService ! Rollback(tId, from, amount, self)
            goto(RollingBackDebit) using pd.copy(retryCount = 0)
        }
    }
  }

  when(RollingBackDebit, config.rollback.timeout) {
    defaultHandler{
    case Event(AccountServiceResponse(RolledBack), _) =>
      val ProcessingData(tId, _, _, _, _, caller) = stateData
      log.info("Rolled back:" + tId)
      caller ! TransferFailed(tId, RolledBackError)
      stop()
    } {
      defaultRetryHandler{
        pd => {
          val ProcessingData(tId, from, _, amount, retryCount, _) = pd
          log.debug("Debit Failed, retrying...[retry=" + {retryCount + 1} + "]:" + tId)
          accountService ! Rollback(tId, from, amount, self)
          stay() using pd.copy(retryCount = retryCount + 1)
        }
      }
    }
  }

  initialize()

  def defaultHandler(handler: StateFunction)
                    (retryHandler:StateFunction): StateFunction = {
    val default: StateFunction = {
      case Event(AccountServiceResponse(ClientNotFound), _) =>
        val ProcessingData(tId, _, _, _, _, caller) = stateData
        log.info("Credited:" + tId)
        caller ! TransferFailed(tId, ClientNotFoundError)
        stop()
      case Event(AccountServiceResponse(AlreadyProcessed), _) =>
        val ProcessingData(tId, _, _, _, _, caller) = stateData
        log.info("Credited:" + tId)
        caller ! TransferFailed(tId, AlreadyProcessedError)
        stop()
    }
    default orElse handler orElse retryHandler
  }

  def defaultRetryHandler(retryHandler: ProcessingData => State): StateFunction = {
    case Event(AccountServiceResponse(_), _) =>
      stateData match {
        case pd@ProcessingData(_, _, _, _, retryCount, _) if retryCount < maxRetryCount =>
          retryHandler(pd)
        case _ =>
          val ProcessingData(tId, _, _, _, _, caller) = stateData
          log.error("Debit failed:" + tId)
          caller ! TransferFailed(tId, UnknownError)
          stop()
      }
  }

}

object TransferProcess {

  def apply(config: TransferProcessConfig, accountService: ActorRef)(implicit system: ActorSystem): ActorRef =
    system.actorOf(Props(new TransferProcess(config, accountService)))

}
