package com.github.michalw.domain.coordinator

import akka.actor.{ActorRef, FSM}
import com.github.michalw.models.{ClientId, Funds, TransactionId}

private[coordinator] sealed trait TransferProcessState
private[coordinator] object TransferProcessState {
  case object Uninitialized extends TransferProcessState
  case object Idle extends TransferProcessState
  case object AwaitingValidationConfirmation extends TransferProcessState
  case object AwaitingDebitConfirmation extends TransferProcessState
  case object AwaitingCreditConfirmation extends  TransferProcessState
  case object RollingBackDebit extends TransferProcessState
}

private[coordinator] sealed trait TransferProcessData
private[coordinator] object TransferProcessData {
  case object NoData extends TransferProcessData
  case class ProcessingData(tId: TransactionId,
                            from: ClientId,
                            to: ClientId,
                            amount: Funds,
                            retryCount: Int,
                            caller: ActorRef) extends TransferProcessData
}

private[coordinator] class TransferProcessFSM extends FSM[TransferProcessState, TransferProcessData]
