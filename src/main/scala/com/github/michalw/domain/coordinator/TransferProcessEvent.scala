package com.github.michalw.domain.coordinator

import com.github.michalw.models.TransferRequest
import com.github.michalw.models.messages.coordinator.TransferStatus

sealed trait TransferProcessEvent

object TransferProcessEvent {
  case class InitTransfer(tId: Long, tr: TransferRequest) extends TransferProcessEvent
  case class AccountServiceResponse(status: TransferStatus) extends TransferProcessEvent
  private [coordinator] case object Debit extends TransferProcessEvent
  private [coordinator] case object Credit extends TransferProcessEvent
  private [coordinator] case object Rollback extends TransferProcessEvent
}
