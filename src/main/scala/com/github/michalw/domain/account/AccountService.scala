package com.github.michalw.domain.account

import scala.concurrent.ExecutionContextExecutor
import akka.actor.{Actor, ActorLogging}
import com.github.michalw.domain.coordinator.TransferProcessEvent.AccountServiceResponse
import com.github.michalw.models.{ClientId, Funds, TransactionId}
import com.github.michalw.models.messages.account._

/**
  * Simple trait to easily implement different type of accounts (local vs remote)
  */
trait AccountService
  extends Actor
    with ActorLogging {

  private implicit val dispatcher: ExecutionContextExecutor = context.dispatcher

  protected def validate(clientIds: Seq[ClientId]): AccountServiceResponse
  protected def credit(tId: TransactionId, clientId: ClientId, funds: Funds): AccountServiceResponse
  protected def debit(tId: TransactionId, clientId: ClientId, funds: Funds): AccountServiceResponse
  protected def rollback(tId: TransactionId, clientId: ClientId, funds: Funds): AccountServiceResponse

  override def receive: Receive = {
    case Validate(_, clientIds, tp) =>
      tp !validate(clientIds)
    case Credit(tId, clientId, funds, tp) =>
      tp !credit(tId, clientId, funds)
    case Debit(tId, clientId, funds, tp) =>
      tp !debit(tId, clientId, funds)
    case Rollback(tId, clientId, funds, tp) =>
      tp !rollback(tId, clientId, funds)
  }

}
