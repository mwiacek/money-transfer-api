package com.github.michalw.domain.account

sealed trait AccountAction
case object DebitAccount extends AccountAction
case object CreditAccount extends AccountAction
case object RollbackAccount extends AccountAction
