package com.github.michalw.domain

import scala.collection.mutable
import com.github.michalw.models.{ClientId, Funds, TransactionId}


package object account {

  type Cache = mutable.HashMap[ClientId, Funds]
  type TransactionKey = (TransactionId, ClientId, AccountAction)
  type TransactionLog = mutable.HashSet[TransactionKey]

}
