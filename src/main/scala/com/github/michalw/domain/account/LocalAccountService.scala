package com.github.michalw.domain.account

import com.github.michalw.domain.coordinator.TransferProcessEvent.AccountServiceResponse
import com.github.michalw.models.{ClientId, Funds, TransactionId}
import com.github.michalw.models.messages.coordinator._

/**
  *
  * @param cache simple cache to mimic storage behaviour
  * @param transactionLog simple set to keep track of transaction steps
  */
class LocalAccountService(private val cache: Cache, private val transactionLog: TransactionLog)
  extends AccountService {

  override def validate(clientIds: Seq[ClientId]): AccountServiceResponse =
    if(cache.keySet.forall(clientIds.contains))
      AccountServiceResponse(ValidClient)
    else
      AccountServiceResponse(ClientNotFound)

  override def credit(tId: TransactionId, clientId: ClientId, funds: Funds): AccountServiceResponse = {
    val action = CreditAccount
    defaultHandler(tId, clientId, action) {
      case current =>
        cache.update(clientId, current + funds)
        transactionLog.add((tId, clientId, action))
        AccountServiceResponse(Credited)
    }
  }

  override def debit(tId: TransactionId, clientId: ClientId, funds: Funds): AccountServiceResponse = {
    val action = DebitAccount
    defaultHandler(tId, clientId, action){
      case current if current < funds =>
        AccountServiceResponse(NotEnoughFunds)
      case current =>
        cache.update(clientId, current - funds)
        transactionLog.add((tId, clientId, action))
        AccountServiceResponse(Debited)
    }
  }

  override def rollback(tId: TransactionId, clientId: ClientId, funds: Funds): AccountServiceResponse = {
    val action = RollbackAccount
    defaultHandler(tId, clientId, action) {
      case current =>
        if (transactionLog.contains((tId, clientId, DebitAccount))) {
          cache.update(clientId, current + funds)
          transactionLog.add((tId, clientId, RollbackAccount))
          AccountServiceResponse(RolledBack)
        } else
          AccountServiceResponse(RollingBackWhenNotDebited)
    }
  }

  private def defaultHandler(tId: TransactionId,
                             clientId: ClientId,
                             accountAction: AccountAction)
                            (handler: PartialFunction[Funds, AccountServiceResponse]): AccountServiceResponse =
    if (transactionLog.contains((tId, clientId, accountAction)))
      AccountServiceResponse(AlreadyProcessed)
    else
      cache.get(clientId).collect{handler}.getOrElse(AccountServiceResponse(ClientNotFound))

}
