package com.github.michalw

import scala.concurrent.ExecutionContextExecutor
import scala.collection.mutable
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives.logRequestResult
import akka.stream.ActorMaterializer
import com.github.michalw.models.{ClientId, Funds}
import com.github.michalw.server.api.TransferRoute
import com.github.michalw.domain.account.{LocalAccountService, TransactionKey}
import com.github.michalw.utils.DataLoader
import com.github.michalw.utils.config.{Environment, EnvironmentConfigUtil, LocalAccountServiceConfig}


object Main extends App with TransferRoute {

  override implicit val system: ActorSystem = ActorSystem("Transfer")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContextExecutor = system.dispatcher

  override val config: Environment = EnvironmentConfigUtil.load()
  private val transactionLog = new mutable.HashSet[TransactionKey]()
  // FIXME: simple solution for running locally
  private val users: mutable.HashMap[ClientId, Funds] =
  config.accountService match {
    case LocalAccountServiceConfig(x) if x.nonEmpty =>
      DataLoader.load(x)
    case _ => new mutable.HashMap[ClientId, Funds]()
  }

  val accountService: ActorRef = system.actorOf(Props(new LocalAccountService(users, transactionLog)))

  Http()
    .bindAndHandle(
      handler = logRequestResult("log")(transfer),
      interface = config.server.host,
      port = config.server.port
    )

}
