package com.github.michalw.utils.config

import scala.concurrent.duration.FiniteDuration


case class Server(host: String, port: Int, timeout: FiniteDuration)
case class Environment(server: Server, accountService: AccountServiceConfig, transferProcess: TransferProcessConfig)
