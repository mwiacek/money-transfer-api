package com.github.michalw.utils.config

import scala.concurrent.duration.FiniteDuration


case class ValidationConfig(timeout: FiniteDuration)
case class CreditConfig(timeout: FiniteDuration)
case class DebitConfig(timeout: FiniteDuration)
case class RollbackConfig(timeout: FiniteDuration)
case class TransferProcessConfig(maxRetryCount: Int,
                                 validation: ValidationConfig,
                                 credit:CreditConfig,
                                 debit:DebitConfig,
                                 rollback:RollbackConfig
                                )
