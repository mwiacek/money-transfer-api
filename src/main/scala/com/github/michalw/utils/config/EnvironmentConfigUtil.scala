package com.github.michalw.utils.config

import com.typesafe.config.{Config, ConfigFactory}
import pureconfig.loadConfig

object EnvironmentConfigUtil {

  def load(resource: String): Environment = {
    val config = ConfigFactory.load(resource)
    load(config)
  }

  def load(): Environment = {
    val config = ConfigFactory.load()
    load(config.getConfig("environment"))
  }
  def load(config: Config): Environment = environmentConfig(config)

  private def environmentConfig(config: Config): Environment =
    loadConfig[Environment](config) match {
      case Right(x) => x
      case Left(value) => throw new Exception(value.toList.mkString("\n")) // FIXME: fix this ugly solution
    }

}
