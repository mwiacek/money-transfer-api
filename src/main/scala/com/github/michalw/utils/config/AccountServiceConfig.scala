package com.github.michalw.utils.config

sealed trait AccountServiceConfig
case class LocalAccountServiceConfig(file: String) extends AccountServiceConfig
