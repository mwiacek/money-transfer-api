package com.github.michalw.utils

import java.util.concurrent.atomic.AtomicLong

trait TransactionIdGen {
  import TransactionIdGen._

  def getTnxId: Long = IdGen.incrementAndGet

}

object TransactionIdGen {

  val IdGen = new AtomicLong

}
