package com.github.michalw.utils

import java.io.InputStream
import scala.collection.mutable
import scala.io.BufferedSource
import scala.util.Try

import com.typesafe.scalalogging.StrictLogging
import com.github.michalw.domain.account.Cache


object DataLoader extends StrictLogging {


  /**
    * Helper to load local storage data from csv file
    * @param file csv file location
    * @return users->funds map
    */
  def load(file: String): Cache = {
    val data: Cache = new mutable.HashMap()
    val stream: InputStream = getClass.getClassLoader.getResourceAsStream(file)
    val bufferedSource: BufferedSource =
      scala.io.Source.fromInputStream(stream)
    val lines = bufferedSource.getLines().drop(1)
    lines.foreach {
      line =>
        Try {
          val (id, funds): (String, String) = line.split(",") match {
            case Array(x, y) => (x, y)
          }
          data.update(id.toLong, funds.toLong)
        }.recover { case _ => logger.error("line:" + line + " has invalid format") } // FIXME: ugly, but just in case
    }
    data
  }
}
