package com.github.michalw.models


case class TransferRequest(from: ClientId, to: ClientId, funds: Funds)
