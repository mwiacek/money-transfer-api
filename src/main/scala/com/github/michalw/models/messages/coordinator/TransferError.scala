package com.github.michalw.models.messages.coordinator


trait TransferError extends Exception
case object ClientNotFoundError extends Exception("client not found") with TransferError
case object AlreadyProcessedError extends Exception("transaction already processed") with TransferError
case object RollingBackNotDebitedError extends Exception("account was not debit") with TransferError
case object NotEnoughFundsError extends Exception("user do not have enough funds") with TransferError
case object RolledBackError extends Exception("transaction was rolled back") with TransferError
case object UnknownError extends Exception("unknown error") with TransferError
