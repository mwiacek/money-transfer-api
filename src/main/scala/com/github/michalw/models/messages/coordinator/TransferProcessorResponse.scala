package com.github.michalw.models.messages.coordinator

import com.github.michalw.models.TransactionId


sealed trait TransferProcessorResponse
case class Transferred(tId: TransactionId) extends TransferProcessorResponse
case class TransferFailed(tId: TransactionId, details: TransferError) extends TransferProcessorResponse
