package com.github.michalw.models.messages.coordinator


sealed trait TransferStatus
case object ValidClient extends TransferStatus
case object Finished extends TransferStatus
case object Credited extends TransferStatus
case object Debited extends TransferStatus
case object RolledBack extends TransferStatus
case object AlreadyProcessed extends TransferStatus
case object RollingBackWhenNotDebited extends TransferStatus
case object NotEnoughFunds extends TransferStatus
case object ClientNotFound extends TransferStatus
