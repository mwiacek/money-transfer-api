package com.github.michalw.models.messages.account

import akka.actor.ActorRef
import com.github.michalw.models.{ClientId, Funds, TransactionId}


sealed trait AccountMessage
case class Validate(tId: TransactionId, clientIds: Seq[ClientId], tp: ActorRef) extends AccountMessage
case class Debit(tId: TransactionId, clientId: ClientId, funds: Funds, tp: ActorRef) extends AccountMessage
case class Credit(tId: TransactionId, clientId: ClientId, funds: Funds, tp: ActorRef) extends AccountMessage
case class Rollback(tId: TransactionId, clientId: ClientId, funds: Funds, tp: ActorRef) extends AccountMessage
