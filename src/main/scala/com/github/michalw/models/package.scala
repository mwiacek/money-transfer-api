package com.github.michalw


package object models {

  type TransactionId = Long
  type ClientId = Long
  type Funds = Long
  type Message = String
  type AccessKey = String

}
