package com.github.michalw.models

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, DerivedFormats, RootJsonFormat}


trait JsonMappings extends SprayJsonSupport with DefaultJsonProtocol with DerivedFormats {
  implicit val preTransferErrorResponseFormat: RootJsonFormat[PreTransferError] = jsonFormat1(PreTransferError)
  implicit val transferResponseFormat: RootJsonFormat[TransferResponse] = jsonFormat2(TransferResponse)
  implicit val transferRequestFormat: RootJsonFormat[TransferRequest] = jsonFormat3(TransferRequest)
}
