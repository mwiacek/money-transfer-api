package com.github.michalw.models


final case class PreTransferError(msg: Message)
final case class TransferResponse(transactionId: TransactionId,
                                  msg: Option[Message] = None)
