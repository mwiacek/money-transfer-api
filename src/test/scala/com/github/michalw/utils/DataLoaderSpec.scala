package com.github.michalw.utils

import scala.collection.mutable
import org.scalatest.{Matchers, WordSpec}


class DataLoaderSpec extends WordSpec with Matchers {

  "Data loader" must {
    "should load file" in {
      val data = DataLoader.load("example-data.csv")
      data shouldBe mutable.Map(2 -> 100, 1 -> 1, 3 -> 1000)
    }
  }

}
