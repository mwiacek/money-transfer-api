package com.github.michalw.server.api

import scala.collection.mutable
import scala.concurrent.ExecutionContextExecutor
import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.StatusCodes._
import akka.actor.{ActorRef, Props}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{TestKit, TestKitBase}
import org.scalatest.{Matchers, WordSpecLike}
import com.github.michalw.domain.account.{LocalAccountService, TransactionKey}
import com.github.michalw.helpers.Helpers
import com.github.michalw.models._
import com.github.michalw.utils.config.{Environment, EnvironmentConfigUtil}


class TransferRouteSpec
  extends WordSpecLike
    with Matchers
    with ScalatestRouteTest
    with TestKitBase
    with TransferRoute {

  override val config: Environment = EnvironmentConfigUtil.load()
  private val transactionLog = new mutable.HashSet[TransactionKey]()
  private val users = new mutable.HashMap[ClientId, Funds]()
  override implicit val ec: ExecutionContextExecutor = system.dispatcher

  override val accountService: ActorRef = system.actorOf(Props(new LocalAccountService(users, transactionLog)))

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "The service" should {
    Helpers.initUserCache(users)
    "should reject transaction if transfering money between same account" in {
      Post("/transfer", TransferRequest(1, 1, 1)) ~> transfer ~> check {
        status shouldBe BadRequest
        contentType shouldBe `application/json`
        responseAs[PreTransferError] shouldBe PreTransferError("cannot transfer funds between same account")
      }
    }
    "should reject transaction if transfering non positive amount of money" in {
      Post("/transfer", TransferRequest(1, 2, 0)) ~> transfer ~> check {
        status shouldBe BadRequest
        contentType shouldBe `application/json`
        responseAs[PreTransferError] shouldBe PreTransferError("only positive value can be transferred between accounts")
      }
    }
    "should be processed if transaction is correct" in {
      Post("/transfer", TransferRequest(1, 2, 1)) ~> transfer ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        responseAs[TransferResponse] shouldBe TransferResponse(1)
        users(1) should be(0)
        users(2) should be(2)
      }
    }
    "should reject transaction if there is not enough money" in {
      Helpers.initUserCache(users)
      users.update(1, 0)
      Post("/transfer", TransferRequest(1, 2, 1)) ~> transfer ~> check {
        status shouldBe MethodNotAllowed
        contentType shouldBe `application/json`
        responseAs[TransferResponse] shouldBe TransferResponse(2, Option("user do not have enough funds"))
        users(1) should be(0)
        users(2) should be(1)
      }
    }
    "should reject transaction if user does not exist" in {
      Helpers.initUserCache(users)
      Post("/transfer", TransferRequest(3, 2, 1)) ~> transfer ~> check {
        status shouldBe NotFound
        contentType shouldBe `application/json`
        responseAs[TransferResponse] shouldBe TransferResponse(3, Option("client not found"))
        users(2) should be(1)
      }
    }
  }
}
