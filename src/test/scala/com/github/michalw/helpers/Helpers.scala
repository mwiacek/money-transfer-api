package com.github.michalw.helpers

import scala.collection.mutable
import akka.actor.{ActorRef, ActorSystem, Props}
import com.github.michalw.domain.account.{Cache, LocalAccountService, TransactionKey, TransactionLog}
import com.github.michalw.domain.coordinator.TransferProcess
import com.github.michalw.models.{ClientId, Funds, TransactionId}
import com.github.michalw.utils.config.TransferProcessConfig


object Helpers {

  private val idGen: Iterator[Int] = Stream.iterate(1)(i => i + 1).iterator

  def createService(implicit system: ActorSystem,
                    tpConfig: TransferProcessConfig): (TransactionId, TransactionLog, Cache, ActorRef) = {
    val id = idGen.next()
    val transactionLog = new mutable.HashSet[TransactionKey]()
    val users = new mutable.HashMap[ClientId, Funds]()
    val actorService = system.actorOf(Props(new LocalAccountService(users, transactionLog)), "actorServiceActor" + id)
    users.update(1, 1)
    users.update(2, 1)
    val tp = system.actorOf(Props(new TransferProcess(tpConfig, actorService)), "tpActor" + id)
    (1, transactionLog, users, tp)
  }

  def initUserCache(users: Cache): Unit = {
    users.clear()
    users.update(1, 1)
    users.update(2, 1)
  }
}
