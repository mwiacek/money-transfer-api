package com.github.michalw.domain.account

import scala.collection.mutable
import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import com.github.michalw.domain.coordinator.TransferProcessEvent.AccountServiceResponse
import com.github.michalw.models.messages.account.{Credit, Debit, Rollback}
import com.github.michalw.models.messages.coordinator._
import com.github.michalw.models.{ClientId, Funds}


class LocalAccountServiceSpec extends TestKit(ActorSystem("LocalAccountServiceSpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "An LocalAccountService actor with no user" must {
    val transactionLog = new mutable.HashSet[TransactionKey]()
    val actorService = system.actorOf(Props(new LocalAccountService(new mutable.HashMap(), transactionLog)), "noUsersService")
    "should reject Credit transaction" in {
      actorService ! Credit(1, 1, 1, this.testActor)
      expectMsg(AccountServiceResponse(ClientNotFound))
    }
    "should reject Debit transaction" in {
      actorService ! Debit(1, 1, 1, this.testActor)
      expectMsg(AccountServiceResponse(ClientNotFound))
    }
    "should reject Rollback transaction" in {
      actorService ! Rollback(1, 1, 1, this.testActor)
      expectMsg(AccountServiceResponse(ClientNotFound))
    }
  }

  "An LocalAccountService with user" must {
    val transactionLog = new mutable.HashSet[TransactionKey]()
    val users = new mutable.HashMap[ClientId, Funds]()
    val cId = 1
    users.update(cId, 1)
    val actorService = system.actorOf(Props(new LocalAccountService(users, transactionLog)), "actorService")
    "should accept valid credit transaction" in {
      actorService ! Credit(1, cId, 1, this.testActor)
      expectMsg(AccountServiceResponse(Credited))
      users(1) should be(2)
    }
    "should accept valid debit transaction" in {
      actorService ! Debit(2, cId, 1, this.testActor)
      expectMsg(AccountServiceResponse(Debited))
      users(1) should be(1)
    }
    "should accept valid rollback transaction" in {
      actorService ! Rollback(2, cId, 1, this.testActor)
      expectMsg(AccountServiceResponse(RolledBack))
      users(1) should be(2)
    }
    "should accept transaction if money runs out" in {
      actorService ! Debit(3, cId, 2, this.testActor)
      expectMsg(AccountServiceResponse(Debited))
      users(1) should be(0)
    }
  }
  "Debit transaction" must {
    val transactionLog = new mutable.HashSet[TransactionKey]()
    val users = new mutable.HashMap[ClientId, Funds]()
    val cId = 1
    users.update(cId, 1)
    val actorService = system.actorOf(Props(new LocalAccountService(users, transactionLog)), "noEnoughMoneyService")
    "should rejected there is not enough money" in {
      actorService ! Debit(1, cId, 2, this.testActor)
      expectMsg(AccountServiceResponse(NotEnoughFunds))
      users(1) should be(1)
    }
    "should rejected if account is empty" in {
      actorService ! Debit(2, cId, 1, this.testActor)
      expectMsg(AccountServiceResponse(Debited))
      actorService ! Debit(3, cId, 1, this.testActor)
      expectMsg(AccountServiceResponse(NotEnoughFunds))
      users(1) should be(0)
    }
  }
  "Rollback transaction" must {
    val transactionLog = new mutable.HashSet[TransactionKey]()
    val users = new mutable.HashMap[ClientId, Funds]()
    val cId = 1
    users.update(cId, 1)
    val actorService = system.actorOf(Props(new LocalAccountService(users, transactionLog)), "rollbackTransactionService")
    "should be rejected if there was not prior debit transaction" in {
      actorService ! Rollback(1, cId, 2, this.testActor)
      expectMsg(AccountServiceResponse(RollingBackWhenNotDebited))
      users(1) should be(1)
    }
    "should be rejected when already processed" in {
      actorService ! Debit(2, cId, 1, this.testActor)
      expectMsg(AccountServiceResponse(Debited))
      actorService ! Rollback(2, cId, 1, this.testActor)
      expectMsg(AccountServiceResponse(RolledBack))
      actorService ! Rollback(2, cId, 1, this.testActor)
      expectMsg(AccountServiceResponse(AlreadyProcessed))
      users(1) should be(1)
    }
  }
}
