package com.github.michalw.domain.coordinator

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.implicitConversions
import scala.language.postfixOps
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import com.github.michalw.domain.account._
import com.github.michalw.domain.coordinator.TransferProcessEvent.InitTransfer
import com.github.michalw.models.messages.coordinator._
import com.github.michalw.models.TransferRequest
import com.github.michalw.utils.config.{Environment, EnvironmentConfigUtil, TransferProcessConfig}


class TransferProcessorSpec
  extends TestKit(ActorSystem("transferProcessorSpec"))
    with ImplicitSender
    with WordSpecLike
    with Matchers
    with BeforeAndAfterAll {

  import com.github.michalw.helpers.Helpers.createService

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  implicit val timeout: Timeout = Timeout(5 seconds)
  val config: Environment = EnvironmentConfigUtil.load()
  implicit val tpConfig: TransferProcessConfig = config.transferProcess

  "Transfer Processor" must {
    "should reject transaction if there is not enough money" in {
      val (tId, transactionLog, users, tp) = createService
      val tr = TransferRequest(1, 2, 2)
      val result = tp ? InitTransfer(tId, tr)
      Await.result(result, 1 seconds) shouldBe TransferFailed(tId, NotEnoughFundsError)
      transactionLog shouldBe Set()
      users(1) should be(1)
      users(2) should be(1)
    }
    "should transfer money correctly if there is enough money" in {
      val (tId, transactionLog, users, tp) = createService
      val tr = TransferRequest(1, 2, 1)
      val result = tp ? InitTransfer(tId, tr)
      Await.result(result, 1 seconds) shouldBe Transferred(tId)
      transactionLog shouldBe Set((tId, 2, CreditAccount), (tId, 1, DebitAccount))
      users(1) should be(0)
      users(2) should be(2)
    }
    "should reject if target client does not exist" in {
      val (tId, _, users, tp) = createService
      val tr = TransferRequest(1, 3, 1)
      val result = tp ? InitTransfer(tId, tr)
      Await.result(result, 1 seconds) shouldBe TransferFailed(tId, ClientNotFoundError)
      users(1) should be(1)
    }
    "should reject if source client does not exist" in {
      val (tId, _, users, tp) = createService
      val tr = TransferRequest(3, 1, 1)
      val result = tp ? InitTransfer(tId, tr)
      Await.result(result, 1 seconds) shouldBe TransferFailed(tId, ClientNotFoundError)
      users(1) should be(1)
    }
    "should reject if same transactionId is used twice" in {
      val (tId, transactionLog, users, tp) = createService
      transactionLog.add((tId, 2, CreditAccount))
      transactionLog.add((tId, 1, DebitAccount))
      val tr = TransferRequest(1, 2, 1)
      val result = tp ? InitTransfer(tId, tr)
      Await.result(result, 1 seconds) shouldBe TransferFailed(tId, AlreadyProcessedError)
      users(1) should be(1)
      users(2) should be(1)
    }
    "should reject if same transactionId is used twice in Credit only" in {
      val (tId, transactionLog, users, tp) = createService
      transactionLog.add((tId, 2, CreditAccount))
      val tr = TransferRequest(1, 2, 1)
      val result = tp ? InitTransfer(tId, tr)
      Await.result(result, 1 seconds) shouldBe TransferFailed(tId, AlreadyProcessedError)
      users(1) should be(0)
      users(2) should be(1)
    }
  }

}
