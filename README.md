#Money transfer REST API

---

This is REST API that implement basic money transfer logic.

Implemented with akka-http akka-actors and akka-fsm.
Transfer Processor is implemented per-request and uses tell-pattern only


Cutoffs:
  
* works only with local service account.

Missing things:

1.  test for timeouts and failures during transfer
2.  proper logging
3.  REST API responses
4.  fixed code duplication
5.  better error handling via either Try or Either
